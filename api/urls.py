from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from . import views


schema_view = get_swagger_view(title='My API')

urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(r'^$', schema_view),
    url(r'^api/1.0/pedidos/tipos/$', views.Tipos_Pedidos.as_view(), name="tipos_pedidos"),
    url(r'^api/1.0/pedidos/tipos/(?P<pk>\d+)/$', views.Tipos_PedidosABM.as_view(), name="tipos_pedido"),
    url(r'^api/1.0/pedidos/$', views.Pedidos.as_view(), name="pedidos"),
    url(r'^api/1.0/pedidos/(?P<pk>\d+)/$', views.PedidosABM.as_view(), name="pedido"),
    url(r'^api/1.0/roles/$', views.Roles.as_view(), name="roles"),
    url(r'^api/1.0/roles/(?P<pk>\d+)/$', views.RolesABM.as_view(), name="role"),
    url(r'^api/1.0/paises/$', views.Paises.as_view(), name="paises"),
    url(r'^api/1.0/paises/(?P<pk>\d+)/$', views.PaisesABM.as_view(), name="pais"),
    url(r'^api/1.0/provincias/$', views.Provincias.as_view(), name="provincias"),
    url(r'^api/1.0/provincias/(?P<pk>\d+)/$', views.ProvinciasABM.as_view(), name="provincia"),
    url(r'^api/1.0/provinciasPorPais/(?P<pk>\d+)/$', views.get_provincias_by_pais, name="provincias_por_pais"),
    url(r'^api/1.0/localidades/$', views.Localidades.as_view(), name="localidades"),
    url(r'^api/1.0/localidades/(?P<pk>\d+)/$', views.LocalidadesABM.as_view(), name="localidad"),
    url(r'^api/1.0/localidadesPorProvincia/(?P<pk>\d+)/$', views.get_localidades_by_provincia, name="localidades_por_provincia"),
    url(r'^api/1.0/devocionales/$', views.Devocionales.as_view(), name="devocionales"),
    url(r'^api/1.0/devocionales/(?P<pk>\d+)/$', views.DevocionalesABM.as_view(), name="devocional"),
    url(r'^api/1.0/usuarios/$', views.Usuarios.as_view(), name="usuarios"),
    url(r'^api/1.0/usuarios/(?P<pk>\d+)/$', views.UsuariosABM.as_view(), name="usuario"),
    url(r'^.*/$', views.error_404, name='error404'),
]

