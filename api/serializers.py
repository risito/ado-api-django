from rest_framework import serializers
from api.models import ENT_Tipo_Pedido, ENT_Pedido, AUX_Pais, SYS_Rol, AUX_Provincia, AUX_Localidad, ENT_Devocional,SYS_Usuario


class TipoPedidosSerializer(serializers.ModelSerializer):

    class Meta:
        model = ENT_Tipo_Pedido
        fields = ('pk', 'descripcion')


class PedidosSerializer(serializers.ModelSerializer):

    class Meta:
        model = ENT_Pedido
        fields = ('pk', 'descripcion', 'fecha', 'id_tipo')


class RolesSerializer(serializers.ModelSerializer):

    class Meta:
        model = SYS_Rol
        fields = ('pk', 'descripcion', 'es_admin')


class PaisesSerializer(serializers.ModelSerializer):

    class Meta:
        model = AUX_Pais
        fields = ('pk', 'descripcion')


class ProvinciasSerializer(serializers.ModelSerializer):

    class Meta:
        model = AUX_Provincia
        fields = ('pk', 'descripcion', 'id_pais')


class LocalidadesSerializer(serializers.ModelSerializer):

    class Meta:
        model = AUX_Localidad
        fields = ('pk', 'descripcion', 'id_provincia')


class DevocionalesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ENT_Devocional
        fields = ('pk', 'fecha', 'pasaje_biblico', 'reflexion')


class UsuariosSerializer(serializers.ModelSerializer):

    class Meta:
        model = SYS_Usuario
        fields = ('pk', 'nombres', 'apellido', 'username', 'password', 'id_localidad', 'calle', 'altura', 'piso_depto',
                  'codigo_postal', 'latitud', 'longitud', 'telefonos', 'mail', 'id_rol', 'fecha_nacimiento')

