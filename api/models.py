from django.db import models


# Create your models here. era automatizacion esta
class ENT_Tipo_Pedido(models.Model):
    descripcion = models.TextField()

    def __str__(self):
        return self.descripcion


class ENT_Pedido(models.Model):
    id_tipo = models.ForeignKey('api.ENT_Tipo_Pedido', on_delete=models.PROTECT)
    descripcion = models.TextField()
    fecha = models.DateField()

    def __str__(self):
        return self.descripcion


class SYS_Rol(models.Model):
    descripcion = models.TextField()
    es_admin = models.BooleanField()

    def __str__(self):
        return self.descripcion


class AUX_Pais(models.Model):
    descripcion = models.TextField()

    def __str__(self):
        return self.descripcion


class AUX_Provincia(models.Model):
    descripcion = models.TextField()
    id_pais = models.ForeignKey('api.AUX_Pais', on_delete=models.PROTECT)

    def __str__(self):
        return self.descripcion


class AUX_Localidad(models.Model):
    descripcion = models.TextField()
    id_provincia = models.ForeignKey('api.AUX_Provincia', on_delete=models.PROTECT)

    def __str__(self):
        return self.descripcion


class ENT_Devocional(models.Model):
    fecha = models.DateField()
    pasaje_biblico = models.TextField()
    reflexion = models.TextField()

    def __str__(self):
        return self.pasaje_biblico


class SYS_Usuario(models.Model):
    nombres = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=50)
    id_localidad = models.ForeignKey('api.AUX_Localidad', on_delete=models.PROTECT)
    calle = models.CharField(max_length=100)
    altura  = models.IntegerField()
    piso_depto = models.CharField(max_length=100)
    codigo_postal = models.IntegerField()
    latitud = models.DecimalField(max_digits=50, decimal_places=5)
    longitud = models.DecimalField(max_digits=50, decimal_places=5)
    telefonos = models.CharField(max_length=100)
    mail = models.CharField(max_length=100)
    id_rol = models.ForeignKey('api.SYS_Rol', on_delete=models.PROTECT)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombres