from django.contrib import admin
from .models import ENT_Tipo_Pedido, ENT_Pedido, SYS_Rol, AUX_Pais, AUX_Provincia,AUX_Localidad,ENT_Devocional,SYS_Usuario


# Registra tus modelos aca....si no los registras aca, no van a aparecer en la base de datos, por mas que tengan un serializer,url y toda la bola
admin.site.register(ENT_Tipo_Pedido)
admin.site.register(ENT_Pedido)
admin.site.register(SYS_Rol)
admin.site.register(AUX_Pais)
admin.site.register(AUX_Provincia)
admin.site.register(AUX_Localidad)
admin.site.register(ENT_Devocional)
admin.site.register(SYS_Usuario)