from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import ENT_Tipo_Pedido, ENT_Pedido, AUX_Pais, SYS_Rol,AUX_Provincia, AUX_Localidad, ENT_Devocional,SYS_Usuario
from rest_framework.decorators import api_view, permission_classes
from .serializers import TipoPedidosSerializer, PedidosSerializer, RolesSerializer, PaisesSerializer, ProvinciasSerializer, LocalidadesSerializer,DevocionalesSerializer,UsuariosSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly


# Solo es esl get y post de tipo pedidos sin pasarle un id
@api_view(['GET'])
def error_404(request):
    return Response({'error': 'Path not found'}, status=status.HTTP_404_NOT_FOUND)


class Tipos_Pedidos(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        tipos = ENT_Tipo_Pedido.objects.all()
        serializer = TipoPedidosSerializer(tipos, many=True)
        return Response(serializer.data)

    # Ejemplo de json -->{"descripcion": "Enfermedad"}
    def post(self, request):
        serializer = TipoPedidosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class Tipos_PedidosABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Tipo_Pedido, pk=pk)
        if model is not None:
            serializer = TipoPedidosSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Tipo_Pedido, pk=pk)
        serializer = TipoPedidosSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(ENT_Tipo_Pedido, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Pedidos(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        pedidos = ENT_Pedido.objects.all()
        serializer = PedidosSerializer(pedidos, many=True)
        return Response(serializer.data)

    # Ejemplo de json -->{"descripcion": "Enfermedad"}
    def post(self, request):
        serializer = PedidosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class PedidosABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Pedido, pk=pk)
        if model is not None:
            serializer = PedidosSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Pedido, pk=pk)
        serializer = PedidosSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(ENT_Pedido, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Roles(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        roles = SYS_Rol.objects.all()
        serializer = RolesSerializer(roles, many=True)
        return Response(serializer.data)

    # Ejemplo de json -->{"descripcion": "Enfermedad"}
    def post(self, request):
        serializer = RolesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class RolesABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(SYS_Rol, pk=pk)
        if model is not None:
            serializer = RolesSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(SYS_Rol, pk=pk)
        serializer = RolesSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(SYS_Rol, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Paises(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        paises = AUX_Pais.objects.all()
        serializer = PaisesSerializer(paises, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PaisesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class PaisesABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Pais, pk=pk)
        if model is not None:
            serializer = PaisesSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Pais, pk=pk)
        serializer = PaisesSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(AUX_Pais, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Provincias(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        provincias = AUX_Provincia.objects.all()
        serializer = ProvinciasSerializer(provincias, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ProvinciasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_provincias_by_pais(request,pk):
    provincias_por_pais = AUX_Provincia.objects.filter(id_pais=pk)
    serializer = ProvinciasSerializer(provincias_por_pais, many=True)
    return Response(serializer.data)


# ABM de tipo de peidos con id
class ProvinciasABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Provincia, pk=pk)
        if model is not None:
            serializer = ProvinciasSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Provincia, pk=pk)
        serializer = ProvinciasSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(AUX_Provincia, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Localidades(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        localidades = AUX_Localidad.objects.all()
        serializer = LocalidadesSerializer(localidades, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = LocalidadesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_localidades_by_provincia(request,pk):
    localidades_por_provincia = AUX_Localidad.objects.filter(id_provincia=pk)
    serializer = LocalidadesSerializer(localidades_por_provincia, many=True)
    return Response(serializer.data)


# ABM de tipo de peidos con id
class LocalidadesABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Localidad, pk=pk)
        if model is not None:
            serializer = LocalidadesSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(AUX_Localidad, pk=pk)
        serializer = LocalidadesSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(AUX_Localidad, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Devocionales(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        roles = ENT_Devocional.objects.all()
        serializer = DevocionalesSerializer(roles, many=True)
        return Response(serializer.data)

    # Ejemplo de json -->{"descripcion": "Enfermedad"}
    def post(self, request):
        serializer = DevocionalesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class DevocionalesABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Devocional, pk=pk)
        if model is not None:
            serializer = DevocionalesSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(ENT_Devocional, pk=pk)
        serializer = DevocionalesSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(ENT_Devocional, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Usuarios(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        roles = SYS_Usuario.objects.all()
        serializer = UsuariosSerializer(roles, many=True)
        return Response(serializer.data)

    # Ejemplo de json -->{"descripcion": "Enfermedad"}
    def post(self, request):
        serializer = UsuariosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# ABM de tipo de peidos con id
class UsuariosABM(APIView):
    """
    put:
        set user password
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        """
        Get
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(SYS_Usuario, pk=pk)
        if model is not None:
            serializer = UsuariosSerializer(model, many=False)
            return Response(serializer.data)

    def put(self, request, pk):
        """
        Put
        :param request:
        :param pk:
        :return:
        """
        model = get_object_or_404(SYS_Usuario, pk=pk)
        serializer = UsuariosSerializer(model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        model = get_object_or_404(SYS_Usuario, pk=pk)
        model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)