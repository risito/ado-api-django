1) Create the project_folder

2) cd project_folder

3) git clone {this repository url} . (ends with a dot)

4) Delete .git folder

5) Open project with Pycharm

6) Create virtualenv

7) pip install -r requirements.txt

8) modify models.py

9) manage.py commands (makemigrations, migrate, createsuperuser)

10) Add git support
